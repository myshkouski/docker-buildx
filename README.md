# Docker Buildx

Use Docker Buildx in Gitlab CI

## Usage

Add to `.gitlab-ci.yml` file
```yaml
include:
  - remote: https://gitlab.com/myshkouski/docker-buildx/-/raw/main/Buildx.gitlab-ci.yml
```
